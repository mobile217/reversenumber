/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.woramet.reverse;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class reverse {

    static void reverseNumber(Integer number []){
        Collections.reverse(Arrays.asList(number));
        System.out.print("Reverse number : ");
        System.out.println(Arrays.toString(number));
    }
    
    public static void main(String[] args) {
        long start, stop;

        start = System.nanoTime();
        Scanner kb =new Scanner(System.in);
        Integer num_list[] = new Integer[10];
        
        for (int i = 0; i < 10; i++) { 
            num_list[i] = kb.nextInt();
        }
        
        System.out.print("Normal number : ");
        System.out.println(Arrays.toString(num_list));
        
        reverseNumber(num_list);
        stop = System.nanoTime();
        System.out.println("Run time of algorithm  = " + (stop - start) * 1E-9 + "secs.");
    }
}
